/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : mz_shop

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2021-03-26 21:09:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `msgnote`
-- ----------------------------
DROP TABLE IF EXISTS `msgnote`;
CREATE TABLE `msgnote` (
  `msguuid` varchar(255) NOT NULL,
  `msgusername` varchar(255) DEFAULT NULL,
  `msguserpicture` varchar(255) DEFAULT NULL,
  `msgdate` varchar(255) DEFAULT NULL,
  `msgcontent` varchar(4500) DEFAULT NULL,
  `msgmainid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`msguuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of msgnote
-- ----------------------------
INSERT INTO `msgnote` VALUES ('2e777cc2-1ba1-4997-a383-b82126b1e85d', '蜂冷查', '/images/tx.jpg', '2021-01-19 14:46', '哈哈', '6a08d40a-7277-4efb-ab84-037bc91fe7a2');
INSERT INTO `msgnote` VALUES ('5e9cba20-bb5e-4fba-89fd-c9dbe00051a7', '断蔽侍并卿', '/images/tx.jpg', '2021-03-03 22:45', 'asdasda', '9f11d558-16c9-48ed-b0f0-1a4f33ea2f05');
INSERT INTO `msgnote` VALUES ('60e879c8-3929-4bda-af77-60dfa9f3cb69', '狗妹番潍', '/images/tx.jpg', '2021-01-19 14:34', '11111', '9f11d558-16c9-48ed-b0f0-1a4f33ea2f05');
INSERT INTO `msgnote` VALUES ('6a08d40a-7277-4efb-ab84-037bc91fe7a2', '若唇巡', '/images/tx.jpg', '2021-01-19 14:46', '哈哈哈 我买了一个手机 非常不错', '');
INSERT INTO `msgnote` VALUES ('87d9ba71-1b22-49ea-901e-20f17020bbea', '砷学', '/images/tx.jpg', '2021-03-03 22:45', '这个店铺淘宝坑', '');
INSERT INTO `msgnote` VALUES ('9f11d558-16c9-48ed-b0f0-1a4f33ea2f05', '枯驶繁万料忿', '/images/tx.jpg', '2021-01-19 14:34', '11111', '');
INSERT INTO `msgnote` VALUES ('a171020e-b3e5-4f23-bba8-178f96d730c8', '盘油五目臃艺抄', '/images/tx.jpg', '2021-01-19 14:41', 'hahahahha', '9f11d558-16c9-48ed-b0f0-1a4f33ea2f05');
INSERT INTO `msgnote` VALUES ('b3241704-bd3c-4be1-89aa-d33b0dcf5360', '勉', '/images/tx.jpg', '2021-03-03 22:45', 'adadad', '');
INSERT INTO `msgnote` VALUES ('b46ace65-a118-4fd0-bf61-38341e1e204c', '炉', '/images/tx.jpg', '2021-03-03 22:45', 'adasdasdasda', 'd08bd77b-1785-41b1-b610-c0e697491bda');
INSERT INTO `msgnote` VALUES ('d08bd77b-1785-41b1-b610-c0e697491bda', '随倔拂', '/images/tx.jpg', '2021-03-03 22:45', 'adasda', '');
INSERT INTO `msgnote` VALUES ('f09ce22e-1825-4516-aa8f-4aff4672acf3', '弦梦舵廓独毙夜', '/images/tx.jpg', '2021-03-03 22:46', '我也觉得', '87d9ba71-1b22-49ea-901e-20f17020bbea');

-- ----------------------------
-- Table structure for `tb_newbee_mall_admin_user`
-- ----------------------------
DROP TABLE IF EXISTS `tb_newbee_mall_admin_user`;
CREATE TABLE `tb_newbee_mall_admin_user` (
  `admin_user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `login_user_name` varchar(50) NOT NULL COMMENT '管理员登陆名称',
  `login_password` varchar(50) NOT NULL COMMENT '管理员登陆密码',
  `nick_name` varchar(50) NOT NULL COMMENT '管理员显示昵称',
  `locked` tinyint(4) DEFAULT '0' COMMENT '是否锁定 0未锁定 1已锁定无法登陆',
  PRIMARY KEY (`admin_user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_newbee_mall_admin_user
-- ----------------------------
INSERT INTO `tb_newbee_mall_admin_user` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '十三', '0');
INSERT INTO `tb_newbee_mall_admin_user` VALUES ('2', 'newbee-admin1', 'e10adc3949ba59abbe56e057f20f883e', '新蜂01', '0');
INSERT INTO `tb_newbee_mall_admin_user` VALUES ('3', 'newbee-admin2', 'e10adc3949ba59abbe56e057f20f883e', '新蜂02', '0');

-- ----------------------------
-- Table structure for `tb_newbee_mall_carousel`
-- ----------------------------
DROP TABLE IF EXISTS `tb_newbee_mall_carousel`;
CREATE TABLE `tb_newbee_mall_carousel` (
  `carousel_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '首页轮播图主键id',
  `carousel_url` varchar(100) NOT NULL DEFAULT '' COMMENT '轮播图',
  `redirect_url` varchar(100) NOT NULL DEFAULT '''##''' COMMENT '点击后的跳转地址(默认不跳转)',
  `carousel_rank` int(11) NOT NULL DEFAULT '0' COMMENT '排序值(字段越大越靠前)',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识字段(0-未删除 1-已删除)',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user` int(11) NOT NULL DEFAULT '0' COMMENT '创建者id',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_user` int(11) NOT NULL DEFAULT '0' COMMENT '修改者id',
  PRIMARY KEY (`carousel_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_newbee_mall_carousel
-- ----------------------------
INSERT INTO `tb_newbee_mall_carousel` VALUES ('2', 'https://newbee-mall.oss-cn-beijing.aliyuncs.com/images/banner1.png', 'https://www.dior.cn/zh_cn', '13', '1', '2021-11-29 00:00:00', '0', '2021-03-04 22:03:41', '0');
INSERT INTO `tb_newbee_mall_carousel` VALUES ('5', 'https://newbee-mall.oss-cn-beijing.aliyuncs.com/images/banner2.png', 'https://juejin.im/book/5da2f9d4f265da5b81794d48/section/5da2f9d6f265da5b794f2189', '0', '1', '2021-11-29 00:00:00', '0', '2021-03-04 22:03:41', '0');
INSERT INTO `tb_newbee_mall_carousel` VALUES ('6', 'http://localhost:8080/upload/20210304_22035786.jpg', '##', '0', '0', '2021-03-04 22:03:59', '0', '2021-03-04 22:03:59', '0');
INSERT INTO `tb_newbee_mall_carousel` VALUES ('7', 'http://localhost:8080/upload/20210304_22040559.jpg', '##', '0', '0', '2021-03-04 22:04:06', '0', '2021-03-04 22:04:06', '0');
INSERT INTO `tb_newbee_mall_carousel` VALUES ('8', 'http://localhost:8080/upload/20210304_22042713.jpg', '##', '0', '0', '2021-03-04 22:04:28', '0', '2021-03-04 22:04:28', '0');

-- ----------------------------
-- Table structure for `tb_newbee_mall_goods_category`
-- ----------------------------
DROP TABLE IF EXISTS `tb_newbee_mall_goods_category`;
CREATE TABLE `tb_newbee_mall_goods_category` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `category_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '分类级别(1-一级分类 2-二级分类 3-三级分类)',
  `parent_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '父分类id',
  `category_name` varchar(50) NOT NULL DEFAULT '' COMMENT '分类名称',
  `category_rank` int(11) NOT NULL DEFAULT '0' COMMENT '排序值(字段越大越靠前)',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识字段(0-未删除 1-已删除)',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user` int(11) NOT NULL DEFAULT '0' COMMENT '创建者id',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_user` int(11) DEFAULT '0' COMMENT '修改者id',
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_newbee_mall_goods_category
-- ----------------------------
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('109', '1', '0', '粉底液', '1', '0', '2021-03-04 22:04:42', '0', '2021-03-04 22:04:42', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('110', '2', '109', '粉底液', '0', '0', '2021-03-04 22:04:47', '0', '2021-03-04 22:04:47', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('111', '3', '110', '粉底液', '0', '0', '2021-03-04 22:04:59', '0', '2021-03-04 22:04:59', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('112', '1', '0', '睫毛膏', '1', '0', '2021-03-04 22:04:42', '0', '2021-03-04 22:04:42', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('113', '2', '113', '睫毛膏', '0', '0', '2021-03-04 22:04:47', '0', '2021-03-04 22:04:47', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('114', '3', '114', '睫毛膏', '0', '0', '2021-03-04 22:04:59', '0', '2021-03-04 22:04:59', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('115', '1', '0', '口红', '1', '0', '2021-03-04 22:04:42', '0', '2021-03-04 22:04:42', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('116', '2', '115', '口红', '0', '0', '2021-03-04 22:04:47', '0', '2021-03-04 22:04:47', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('117', '3', '116', '口红', '0', '0', '2021-03-04 22:04:59', '0', '2021-03-04 22:04:59', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('118', '1', '0', '气垫', '1', '0', '2021-03-04 22:04:42', '0', '2021-03-04 22:04:42', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('119', '2', '118', '气垫', '0', '0', '2021-03-04 22:04:47', '0', '2021-03-04 22:04:47', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('120', '3', '119', '气垫', '0', '0', '2021-03-04 22:04:59', '0', '2021-03-04 22:04:59', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('121', '1', '0', '散粉', '1', '0', '2021-03-04 22:04:42', '0', '2021-03-04 22:04:42', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('122', '2', '121', '散粉', '0', '0', '2021-03-04 22:04:47', '0', '2021-03-04 22:04:47', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('123', '3', '122', '散粉', '0', '0', '2021-03-04 22:04:59', '0', '2021-03-04 22:04:59', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('124', '1', '0', '修容盘', '1', '0', '2021-03-04 22:04:42', '0', '2021-03-04 22:04:42', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('125', '2', '124', '修容盘', '0', '0', '2021-03-04 22:04:47', '0', '2021-03-04 22:04:47', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('126', '3', '125', '修容盘', '0', '0', '2021-03-04 22:04:59', '0', '2021-03-04 22:04:59', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('127', '1', '0', '眼线笔', '1', '0', '2021-03-04 22:04:42', '0', '2021-03-04 22:04:42', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('128', '2', '127', '眼线笔', '0', '0', '2021-03-04 22:04:47', '0', '2021-03-04 22:04:47', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('129', '3', '128', '眼线笔', '0', '0', '2021-03-04 22:04:59', '0', '2021-03-04 22:04:59', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('130', '1', '0', '眼影盘', '1', '0', '2021-03-04 22:04:42', '0', '2021-03-04 22:04:42', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('131', '2', '130', '眼影盘', '0', '0', '2021-03-04 22:04:47', '0', '2021-03-04 22:04:47', '0');
INSERT INTO `tb_newbee_mall_goods_category` VALUES ('132', '3', '131', '眼影盘', '0', '0', '2021-03-04 22:04:59', '0', '2021-03-04 22:04:59', '0');

-- ----------------------------
-- Table structure for `tb_newbee_mall_goods_info`
-- ----------------------------
DROP TABLE IF EXISTS `tb_newbee_mall_goods_info`;
CREATE TABLE `tb_newbee_mall_goods_info` (
  `goods_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品表主键id',
  `goods_name` varchar(200) NOT NULL DEFAULT '' COMMENT '商品名',
  `goods_intro` varchar(200) NOT NULL DEFAULT '' COMMENT '商品简介',
  `goods_category_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '关联分类id',
  `goods_cover_img` varchar(200) NOT NULL DEFAULT '/admin/dist/img/no-img.png' COMMENT '商品主图',
  `goods_carousel` varchar(500) NOT NULL DEFAULT '/admin/dist/img/no-img.png' COMMENT '商品轮播图',
  `goods_detail_content` text NOT NULL COMMENT '商品详情',
  `original_price` float(11,5) NOT NULL DEFAULT '1.00000' COMMENT '商品价格',
  `selling_price` float(11,5) NOT NULL DEFAULT '1.00000' COMMENT '商品实际售价',
  `stock_num` int(11) NOT NULL DEFAULT '0' COMMENT '商品库存数量',
  `tag` varchar(20) NOT NULL DEFAULT '' COMMENT '商品标签',
  `goods_sell_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '商品上架状态 0-下架 1-上架',
  `create_user` int(11) NOT NULL DEFAULT '0' COMMENT '添加者主键id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '商品添加时间',
  `update_user` int(11) NOT NULL DEFAULT '0' COMMENT '修改者主键id',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '商品修改时间',
  PRIMARY KEY (`goods_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10907 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_newbee_mall_goods_info
-- ----------------------------
INSERT INTO `tb_newbee_mall_goods_info` VALUES ('10897', '粉底液', '粉底液', '111', 'http://localhost:8080/upload/20210304_22125046.jpg', 'http://localhost:8080/upload/20210304_22125046.jpg', '粉底液', '100.00000', '100.00000', '80', '粉底液', '0', '0', '2021-03-04 22:12:51', '0', '2021-03-04 22:12:51');
INSERT INTO `tb_newbee_mall_goods_info` VALUES ('10898', '粉底液', '粉底液', '111', 'http://localhost:8080/upload/20210304_22131030.jpg', 'http://localhost:8080/upload/20210304_22131030.jpg', '粉底液', '1.00000', '1.00000', '0', '粉底液', '0', '0', '2021-03-04 22:13:11', '0', '2021-03-04 22:13:11');
INSERT INTO `tb_newbee_mall_goods_info` VALUES ('10899', '粉底液', '粉底液', '111', 'http://localhost:8080/upload/20210304_22132150.jpg', 'http://localhost:8080/upload/20210304_22132150.jpg', '粉底液', '1.00000', '1.00000', '0', '粉底液', '0', '0', '2021-03-04 22:13:22', '0', '2021-03-04 22:13:22');
INSERT INTO `tb_newbee_mall_goods_info` VALUES ('10900', '粉底液', '粉底液', '111', 'http://localhost:8080/upload/20210304_22133364.jpg', 'http://localhost:8080/upload/20210304_22133364.jpg', '粉底液', '1.00000', '1.00000', '0', '粉底液', '0', '0', '2021-03-04 22:13:34', '0', '2021-03-04 22:13:34');
INSERT INTO `tb_newbee_mall_goods_info` VALUES ('10901', '粉底液', '粉底液', '111', 'http://localhost:8080/upload/20210304_22134727.jpg', 'http://localhost:8080/upload/20210304_22134727.jpg', '粉底液', '1.00000', '1.00000', '0', '粉底液', '0', '0', '2021-03-04 22:13:48', '0', '2021-03-04 22:13:48');
INSERT INTO `tb_newbee_mall_goods_info` VALUES ('10902', '口红', '口红', '117', 'http://localhost:8080/upload/20210304_22154172.jpg', 'http://localhost:8080/upload/20210304_22154172.jpg', '口红', '1.00000', '1.00000', '0', '口红', '0', '0', '2021-03-04 22:15:42', '0', '2021-03-04 22:15:42');
INSERT INTO `tb_newbee_mall_goods_info` VALUES ('10903', '口红', '口红', '111', 'http://localhost:8080/upload/20210304_22155144.jpg', 'http://localhost:8080/upload/20210304_22155144.jpg', '口红', '1.00000', '1.00000', '0', '口红', '0', '0', '2021-03-04 22:15:52', '0', '2021-03-04 22:15:52');
INSERT INTO `tb_newbee_mall_goods_info` VALUES ('10904', '口红', '口红', '111', 'http://localhost:8080/upload/20210304_22160334.jpg', 'http://localhost:8080/upload/20210304_22160334.jpg', '口红', '1.00000', '1.00000', '0', '口红', '0', '0', '2021-03-04 22:16:30', '0', '2021-03-04 22:16:30');
INSERT INTO `tb_newbee_mall_goods_info` VALUES ('10905', '口红', '口红', '117', 'http://localhost:8080/upload/20210304_2223514.jpg', 'http://localhost:8080/upload/20210304_2223514.jpg', '口红', '1.00000', '1.00000', '0', '口红', '0', '0', '2021-03-04 22:16:53', '0', '2021-03-04 22:23:52');
INSERT INTO `tb_newbee_mall_goods_info` VALUES ('10906', '口红', '口红', '117', 'http://localhost:8080/upload/20210304_22211033.jpg', 'http://localhost:8080/upload/20210304_22211033.jpg', '口红', '1.00000', '1.00000', '0', '口红', '0', '0', '2021-03-04 22:21:11', '0', '2021-03-04 22:21:11');

-- ----------------------------
-- Table structure for `tb_newbee_mall_index_config`
-- ----------------------------
DROP TABLE IF EXISTS `tb_newbee_mall_index_config`;
CREATE TABLE `tb_newbee_mall_index_config` (
  `config_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '首页配置项主键id',
  `config_name` varchar(50) NOT NULL DEFAULT '' COMMENT '显示字符(配置搜索时不可为空，其他可为空)',
  `config_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1-搜索框热搜 2-搜索下拉框热搜 3-(首页)热销商品 4-(首页)新品上线 5-(首页)为你推荐',
  `goods_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品id 默认为0',
  `redirect_url` varchar(100) NOT NULL DEFAULT '##' COMMENT '点击后的跳转地址(默认不跳转)',
  `config_rank` int(11) NOT NULL DEFAULT '0' COMMENT '排序值(字段越大越靠前)',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识字段(0-未删除 1-已删除)',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user` int(11) NOT NULL DEFAULT '0' COMMENT '创建者id',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最新修改时间',
  `update_user` int(11) DEFAULT '0' COMMENT '修改者id',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_newbee_mall_index_config
-- ----------------------------
INSERT INTO `tb_newbee_mall_index_config` VALUES ('25', '粉底液', '3', '10897', '##', '0', '0', '2021-03-04 22:14:15', '0', '2021-03-04 22:14:15', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('26', '粉底液', '3', '10898', '##', '0', '0', '2021-03-04 22:14:29', '0', '2021-03-04 22:14:29', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('27', '粉底液', '3', '10899', '##', '0', '0', '2021-03-04 22:14:36', '0', '2021-03-04 22:14:36', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('28', '粉底液', '3', '10900', '##', '0', '0', '2021-03-04 22:14:47', '0', '2021-03-04 22:14:47', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('29', '粉底液', '3', '10901', '##', '0', '0', '2021-03-04 22:14:57', '0', '2021-03-04 22:14:57', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('30', '口红', '4', '10902', '##', '3', '0', '2021-03-04 22:22:36', '0', '2021-03-04 22:22:36', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('31', '口红', '4', '10903', '##', '1', '0', '2021-03-04 22:22:48', '0', '2021-03-04 22:22:48', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('32', '口红', '4', '10904', '##', '2', '0', '2021-03-04 22:23:00', '0', '2021-03-04 22:23:00', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('33', '口红', '4', '10906', '##', '4', '0', '2021-03-04 22:23:07', '0', '2021-03-04 22:23:07', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('34', '口红', '4', '10907', '##', '0', '0', '2021-03-04 22:23:19', '0', '2021-03-04 22:23:19', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('35', '粉底液', '4', '10901', '##', '5', '0', '2021-03-04 22:24:58', '0', '2021-03-04 22:24:58', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('36', '粉底液', '5', '10897', '##', '0', '0', '2021-03-04 22:27:01', '0', '2021-03-04 22:27:01', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('37', '粉底液', '5', '10898', '##', '5', '0', '2021-03-04 22:27:01', '0', '2021-03-04 22:27:01', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('38', '粉底液', '5', '10899', '##', '4', '0', '2021-03-04 22:27:01', '0', '2021-03-04 22:27:01', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('39', '粉底液', '5', '10900', '##', '3', '0', '2021-03-04 22:27:01', '0', '2021-03-04 22:27:01', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('40', '粉底液', '5', '10901', '##', '2', '0', '2021-03-04 22:27:01', '0', '2021-03-04 22:27:01', '0');
INSERT INTO `tb_newbee_mall_index_config` VALUES ('41', '粉底液', '5', '10902', '##', '1', '0', '2021-03-04 22:27:01', '0', '2021-03-04 22:27:01', '0');

-- ----------------------------
-- Table structure for `tb_newbee_mall_order`
-- ----------------------------
DROP TABLE IF EXISTS `tb_newbee_mall_order`;
CREATE TABLE `tb_newbee_mall_order` (
  `order_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '订单表主键id',
  `order_no` varchar(20) NOT NULL DEFAULT '' COMMENT '订单号',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户主键id',
  `total_price` int(11) NOT NULL DEFAULT '1' COMMENT '订单总价',
  `pay_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '支付状态:0.未支付,1.支付成功,-1:支付失败',
  `pay_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0.无 1.支付宝支付 2.微信支付',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `order_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '订单状态:0.待支付 1.已支付 2.配货完成 3:出库成功 4.交易成功 -1.手动关闭 -2.超时关闭 -3.商家关闭',
  `extra_info` varchar(100) NOT NULL DEFAULT '' COMMENT '订单body',
  `user_name` varchar(30) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `user_phone` varchar(11) NOT NULL DEFAULT '' COMMENT '收货人手机号',
  `user_address` varchar(100) NOT NULL DEFAULT '' COMMENT '收货人收货地址',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识字段(0-未删除 1-已删除)',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最新修改时间',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_newbee_mall_order
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_newbee_mall_order_item`
-- ----------------------------
DROP TABLE IF EXISTS `tb_newbee_mall_order_item`;
CREATE TABLE `tb_newbee_mall_order_item` (
  `order_item_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '订单关联购物项主键id',
  `order_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '订单主键id',
  `goods_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '关联商品id',
  `goods_name` varchar(200) NOT NULL DEFAULT '' COMMENT '下单时商品的名称(订单快照)',
  `goods_cover_img` varchar(200) NOT NULL DEFAULT '' COMMENT '下单时商品的主图(订单快照)',
  `selling_price` int(11) NOT NULL DEFAULT '1' COMMENT '下单时商品的价格(订单快照)',
  `goods_count` int(11) NOT NULL DEFAULT '1' COMMENT '数量(订单快照)',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`order_item_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_newbee_mall_order_item
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_newbee_mall_shopping_cart_item`
-- ----------------------------
DROP TABLE IF EXISTS `tb_newbee_mall_shopping_cart_item`;
CREATE TABLE `tb_newbee_mall_shopping_cart_item` (
  `cart_item_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '购物项主键id',
  `user_id` bigint(20) NOT NULL COMMENT '用户主键id',
  `goods_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '关联商品id',
  `goods_count` int(11) NOT NULL DEFAULT '1' COMMENT '数量(最大为5)',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识字段(0-未删除 1-已删除)',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最新修改时间',
  PRIMARY KEY (`cart_item_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_newbee_mall_shopping_cart_item
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_newbee_mall_user`
-- ----------------------------
DROP TABLE IF EXISTS `tb_newbee_mall_user`;
CREATE TABLE `tb_newbee_mall_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户主键id',
  `nick_name` varchar(50) NOT NULL DEFAULT '' COMMENT '用户昵称',
  `login_name` varchar(11) NOT NULL DEFAULT '' COMMENT '登陆名称(默认为手机号)',
  `password_md5` varchar(32) NOT NULL DEFAULT '' COMMENT 'MD5加密后的密码',
  `introduce_sign` varchar(100) NOT NULL DEFAULT '' COMMENT '个性签名',
  `address` varchar(100) NOT NULL DEFAULT '' COMMENT '收货地址',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '注销标识字段(0-正常 1-已注销)',
  `locked_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '锁定标识字段(0-未锁定 1-已锁定)',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_newbee_mall_user
-- ----------------------------
INSERT INTO `tb_newbee_mall_user` VALUES ('1', '13377952856', '13377952856', 'e10adc3949ba59abbe56e057f20f883e', '', '', '0', '0', '2021-03-16 23:54:54');
